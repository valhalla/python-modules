=====================================
 Debian Python Modules Team - Policy
=====================================

:Author: Gustavo Franco <stratus@debian.org>, Raphaël Hertzog <hertzog@debian.org>, Barry Warsaw <barry@debian.org>
:License: GNU GPL v2 or later

:Introduction:
  The Debian Python Modules Team (DPMT) aims to improve the Python modules
  situation in Debian, by packaging available modules that may be useful and
  providing a central location for packages maintained by a team, hence
  improving responsiveness, integration, and standardization.

  The DPMT is hosted at alioth.debian.org, the Debian GForge installation. We
  currently have a git repository and a mailing list whose email address can
  be used in the ``Maintainer`` or ``Uploaders`` fields on co-maintained
  packages.

  For more information send a message to: debian-python@lists.debian.org

.. contents::


Joining the team
================

The team is open to any Python-related package maintainer. To be added to
the team, please send your request to debian-python@lists.debian.org. Include
the following in your request:

* Why you want to join the team: e.g. maintain your current packages within
  the team, help maintain some specific packages, etc.
* Your Alioth login.
* A statement that you have read
  https://python-modules.alioth.debian.org/policy.html and that you accept it.

Any Debian developer who wishes to integrate their packages in the team can do
so without requesting access (as the repository is writable by all DD). If one
wants to be more involved in the team, we still recommend requesting_ access
so that they appear in the public member list displayed on Alioth's project
page.

The team accepts all contributors and is not restricted to Debian developers.
Several Debian developers of the team will gladly sponsor packages of non-DD
who are part of the team. Sponsorship requests can be sent on the main
discussion list or on #debian-python IRC channel (OFTC network).

All team members should of course follow the main discussion list:
debian-python@lists.debian.org


Maintainership
==============

A package maintained within the team should have the name of the team either
in the ``Maintainer`` field or in the ``Uploaders`` field.

Maintainer: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>

This enables the team to have an overview of its packages on the DDPO_website_.

* Team in ``Maintainers`` is a strong statement that fully collaborative
  maintenance is preferred. Anyone can commit to the git repository and upload
  as needed. A courtesy email to ``Uploaders`` can be nice but not required.

* Team in ``Uploaders`` is a weak statement of collaboration. Help in
  maintaining the package is appreciated, commits to the git repository are
  freely welcomed, but before uploading, please contact the ``Maintainer`` for
  the green light.

Team members who have broad interest should subscribe to the mailing list
python-modules-team@lists.alioth.debian.org whereas members who are only
interested in some packages should use the Package Tracking System to
follow the packages.


Git Procedures
==============

As of October 9, 2015, the DPMT uses git as the version control system for all
packages, and git-dpm as the patch management regime.

Git repositories live on Alioth under the url
``git+ssh://git.debian.org/git/python-modules/packages/<src-pkg-name>.git``
To access any source package's repository, use ``gbp clone`` on the above url,
substituting *src-pkg-name* for the source package name of the package in
question.

DPMT git repos are **source-full**, meaning they contain both the upstream
source and the ``debian/`` directory.

DPMT requires a pristine-tar branch, and only upstream tarballs can be used to
advance the upstream branch. Complete upstream git history should be avoided
in the upstream branch.

Deviations from this policy are strongly discouraged. When you must (not want
to) deviate, you MUST include a ``debian/README.source`` file explaining the
rationale for the deviation and the details of working with the package's git
repo.


Tools
-----

Do *not* create ``quilt`` patches directly. Instead, use ``git-dpm
checkout-patched`` to enter a patch branch, edit the files, commit them, and
then use ``git-dpm update-patches`` to switch back to the master branch, with
the commits turned into patches. Use the pseudo header ``Patch-Name`` in the
commit message to control what the patch file will be named, e.g.::

    Patch-Name: fix-the-foo.patch

Use ``git-dpm tag`` to tag the repository when you upload a new release of the
package. See below for the required tag format.


Branch names
------------

Currently, we require the following branch names in the git repo:

* master - The Debianized upstream source directory. This contains both the
  upstream source and a ``debian/`` packaging directory.
* pristine-tar - Contains the standard ``pristine-tar`` deltas.
* upstream - The un-Debianized upstream source. This is what you get when you
  unpack the upstream tarball.

In some cases you may need to have additional branches, such as if you have
deltas for downstream distributions (e.g. Ubuntu) or need to maintain multiple
versions for security releases in previous versions of Debian. In those
cases, follow the `DEP-14 <http://dep.debian.net/deps/dep14/>`__ guidelines,
and document them in ``debian/README.source``.


Tag format
----------

Use the following ``git-dpm`` tag formats for the three branches named above.
These lines at the *end* of the ``debian/.git-dpm`` file will ensure the
correct tag format for the repository::

    debianTag="debian/%e%v"
    patchedTag="patched/%e%v"
    upstreamTag="upstream/%e%u"


More information
----------------

Additional information for working with DPMT git repositories, creating new
repositories, common workflows, and helpful hints, can be found on the
`Debian wiki <https://wiki.debian.org/Python/GitPackaging>`__
page.


Quality Assurance
=================

The goal of the team is to maintain all packages as best as possible.
Thus every member is encouraged to do general QA work on all the
packages: fix bugs, test packages, improve them to use the latest python
packaging tools, etc.


License
=======

Copyright (c) 2005-2015 Debian Python Modules Team. All rights reserved.
This document is free software; you may redistribute it and/or modify
it under the same terms as GNU GPL v2 or later.

.. _requesting: https://alioth.debian.org/project/request.php?group_id=30714
.. _DDPO_website: http://qa.debian.org/developer.php?login=python-modules-team@lists.alioth.debian.org
